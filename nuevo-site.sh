#!/bin/bash

#comprobar parátros
if [[ "$1" == "" ]] ; then
	echo "usage:$0 <project name> <host>"
	exit
fi

project_name=$1
site=$2
projects_home=/home/carles/Proyectos/emfasi
project_home=$projects_home/$project_name
www_home=/home/carles/Sites
db_user=root
db_pass=root
here=`pwd`
vhost=emfasi.drupal.ejemplo.conf
user=carles
alias_file=emfasi.aliases.drushrc.php



echo "Generando $site"

#crear directorios
mkdir                $project_home
chown -R $user:$user $project_home
mkdir                $www_home/$site
chown -R $user:$user $www_home/$site
mkdir                $www_home/$site/logs
chown -R $user:$user $www_home/$site/logs
mkdir                $www_home/$site/misc
chown -R $user:$user $www_home/$site/misc
#crear sites para la navegación sincronizada en el ftp
mkdir -p $project_home/sites/default/files
mkdir -p $project_home/sites/all
mkdir -p $project_home/sites/all/modules
mkdir -p $project_home/sites/all/themes

#enlazar el proyecto y el site
ln -s  $project_home $www_home/$site/htdocs
rm $project_home/$project_name
chown -R $user:$user $www_home/$site/htdocs

#permisos de usuario y apache
chown -R $user:www-data $project_home/*          ;  chmod -R ug+rw $project_home/*
chown -R $user:www-data $project_home/.gitignore ;  chmod -R ug+rw $project_home/.gitignore
chown -R $user:www-data $project_home/.htaccess  ;  chmod -R ug+rw $project_home/.htaccess

#generar virtualhost
echo "cat $here/vhosts/$vhost | sed -e "s/example.com/$site/g" > /etc/apache2/sites-available/$site.conf"
cat $here/vhosts/$vhost | sed -e "s/example.com/$site/g" > /etc/apache2/sites-available/$site.conf
#habilitar $site en apache
a2ensite $site.conf
service apache2 reload

#crear un usuario y darle permisos
echo "Creando usuario y base de datos $db_name"
echo "CREATE USER '$project_name'@'localhost' IDENTIFIED BY '$project_name';" 
echo "CREATE USER '$project_name'@'localhost' IDENTIFIED BY '$project_name';" | mysql -u $db_user -p"$db_pass"
echo "GRANT RELOAD , SHOW DATABASES ON *.* TO '$project_name'@'localhost' IDENTIFIED BY '$project_name' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"
echo "GRANT RELOAD , SHOW DATABASES ON *.* TO '$project_name'@'localhost' IDENTIFIED BY '$project_name' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;" | mysql -u $db_user -p"$db_pass"
echo "CREATE DATABASE IF NOT EXISTS \`$project_name\` ;" 
echo "CREATE DATABASE IF NOT EXISTS \`$project_name\` ;" | mysql -u $db_user -p"$db_pass"
echo "GRANT ALL PRIVILEGES ON \`$project_name\`.* TO '$project_name'@'localhost';" 
echo "GRANT ALL PRIVILEGES ON \`$project_name\`.* TO '$project_name'@'localhost';" | mysql -u $db_user -p"$db_pass"
echo "GRANT ALL PRIVILEGES ON \`$project_name\_%\`.* TO '$project_name'@'localhost';" 
echo "GRANT ALL PRIVILEGES ON \`$project_name\_%\`.* TO '$project_name'@'localhost';" | mysql -u $db_user -p"$db_pass"



#hosts
if grep -q "$site" /etc/hosts
then
  echo "host exists in /etc/hosts"
else
  echo "
127.0.0.1 $site www.$site
" >> /etc/hosts
fi

#hosts fnx.*.com
if grep -q "fnx.$project_name" /etc/hosts
then
  echo "host exists in /etc/hosts"
else
  echo "
83.48.18.126 $project_name.emfasi.local fnx.$project_name.com
" >> /etc/hosts
fi

#generar alias de drush
#echo "cat $here/alias/$alias_file | sed -e "s/\{project_name\}/$project_name/g" | sed -e "s/\{project_home\}/$project_home/g | sed -e "s/\{project_uri\}/$site/g " > ~/.drush/mfs-$project_name.aliases.drushrc.php"
echo "cat $here/alias/$alias_file | sed -e "s/{project_name}/$project_name/g" | sed -e "s/{project_home}/$project_home/g" | sed -e "s/{project_uri}/$site/g""
cat $here/alias/$alias_file | sed -e "s#{project_name}#$project_name#g" | sed -e "s#{project_home}#$project_home#g" | sed -e "s#{project_uri}#$site#g" > ~/.drush/mfs-$project_name.aliases.drushrc.php



#generar settings.php
drupal_db_host=localhost
echo "generando settings.php ... $(cat $here/drupal/settings.php | sed -e "s/{drupal_db_name}/$project_name/g" | sed -e "s/{drupal_db_user}/$project_name/g" | sed -e "s/{drupal_db_pass}/$project_name/g" | sed  -e "s/{drupal_db_host}/$drupal_db_host/g" >  $project_home/sites/default/settings.php)"

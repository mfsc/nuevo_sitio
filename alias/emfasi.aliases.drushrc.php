<?php

/* {project_name} */
$aliases['dev'] = array(
  'root' => '{project_home}',
  'uri' => '{project_uri}',
);


$aliases['fenix'] = array(
  'root' => '/var/www/{project_name}',
  'uri' => '{project_name}.emfasi.local',
  'remote-host' => '{project_name}.emfasi.local',
  'remote-user' => 'root'
);

$aliases['fnx'] = array(
  'root' => '/var/www/{project_name}',
  'uri' => 'fnx.{project_name}.com',
  'remote-host' => 'fnx.{project_name}.com',
  'remote-user' => 'root'
);

$aliases['md'] = array(
  'root' => '/var/www/md-{project_name}',
  'uri' => 'md-{project_name}.emfasi.local',
  'remote-host' => 'md-{project_name}.emfasi.local',
  'remote-user' => 'root'
);

$aliases['emfasi.es'] = array(
  'root' => '/emfasi.es/site47-{project_name}',
  'uri' => '{project_name}.emfasi.es',
  'remote-host' => '{project_name}.emfasi.es',
  'remote-user' => 'root'
);
